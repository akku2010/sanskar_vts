import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';
declare var google;

@IonicPage()
@Component({
  selector: 'page-distance-report',
  templateUrl: 'distance-report.html',
})
export class DistanceReportPage implements OnInit {

  locationAddress: any;
  locationEndAddress: any;
  distanceReport: any;
  Ignitiondevice_id: any;
  datetimeEnd: string;
  datetimeStart: string;
  islogin: any;
  // devices1243: any[];
  devices: any;
  portstemp: any;
  datetime: number;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apicallDistance: ApiServiceProvider,
    public toastCtrl: ToastController
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    // this.datetimeStart = moment({ hours: 0 }).format();
    // this.datetimeEnd = moment().format();//new Date(a).toISOString();

    this.datetimeStart = moment({ hours: 0 }).subtract(1, 'days').format(); // yesterday date with 12:00 am
    console.log("today time: ", this.datetimeStart)
    this.datetimeEnd = moment({ hours: 0 }).format(); // today date and time with 12:00am
  }

  ngOnInit() {
    this.getdevices();
  }

  getdevices() {
    var baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.apicallDistance.startLoading().present();
    this.apicallDistance.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apicallDistance.stopLoading();
        this.devices = data;
        this.portstemp = data.devices;
      },
        err => {
          this.apicallDistance.stopLoading();
          console.log(err);
        });
  }

  getdistancedevice(selectedVehicle) {
    console.log("selectedVehicle=> ", selectedVehicle)
    this.Ignitiondevice_id = selectedVehicle._id;
  }

  distanceReportData = [];
  getDistanceReport() {
    if (this.Ignitiondevice_id == undefined) {
      this.Ignitiondevice_id = "";
    }
    let outerthis = this;
    this.apicallDistance.startLoading().present();
    this.apicallDistance.getDistanceReportApi(new Date(outerthis.datetimeStart).toISOString(), new Date(outerthis.datetimeEnd).toISOString(), this.islogin._id, this.Ignitiondevice_id)
      .subscribe(data => {
        this.apicallDistance.stopLoading();
        this.distanceReport = data;
        if (this.distanceReport.length > 0) {
          this.innerFunc(this.distanceReport);
        } else {
          let toast = this.toastCtrl.create({
            message: 'Report(s) not found for selected dates/Vehicle.',
            duration: 1500,
            position: 'bottom'
          })
          toast.present();
        }
      }, error => {
        this.apicallDistance.stopLoading();
        console.log(error);
      })
  }

  innerFunc(distanceReport) {
    let outerthis = this;
    var i = 0, howManyTimes = distanceReport.length;
    function f() {

      outerthis.distanceReportData.push({
        'distance': outerthis.distanceReport[i].distance,
        'Device_Name': outerthis.distanceReport[i].device.Device_Name
      });
      if (outerthis.distanceReport[i].endLat != null && outerthis.distanceReport[i].startLat != null) {
        var latEnd = outerthis.distanceReport[i].endLat;
        var lngEnd = outerthis.distanceReport[i].endLng;
        var latlng = new google.maps.LatLng(latEnd, lngEnd);

        var latStart = outerthis.distanceReport[i].startLat;
        var lngStart = outerthis.distanceReport[i].startLng;


        var lngStart1 = new google.maps.LatLng(latStart, lngStart);
        var geocoder = new google.maps.Geocoder();

        var request = {
          latLng: latlng
        };
        var request1 = {
          latLng: lngStart1
        };
        geocoder.geocode(request, function (data, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            if (data[1] != null) {
              outerthis.locationEndAddress = data[1].formatted_address;
            }
          }
          outerthis.distanceReportData[outerthis.distanceReportData.length - 1].EndLocation = outerthis.locationEndAddress;
        })

        geocoder.geocode(request1, function (data, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            if (data[1] != null) {
              outerthis.locationAddress = data[1].formatted_address;
            }
          }
          outerthis.distanceReportData[outerthis.distanceReportData.length - 1].StartLocation = outerthis.locationAddress;
        })
      } else {
        outerthis.distanceReportData[outerthis.distanceReportData.length - 1].StartLocation = 'N/A';
        outerthis.distanceReportData[outerthis.distanceReportData.length - 1].EndLocation = 'N/A';
      }
      console.log(outerthis.distanceReportData);

      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }

    }
    f();
  }

}
