import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VehicleDetailsPage } from './vehicle-details';
import { IonBottomDrawerModule } from 'ion-bottom-drawer';

@NgModule({
  declarations: [
    VehicleDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(VehicleDetailsPage),
    IonBottomDrawerModule
  ]
})
export class VehicleDetailsPageModule {}